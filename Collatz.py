#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------


def collatz_eval(i, j, cache):          # added cache as a parameter
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    assert type(i) and type(j) == int
    if j < i:                           # flip the inputs so that i < j
        temp = i
        i = j
        j = temp
    j = j+1                             # expand range to include end, j

    longest = 1
    for x in range(i, j):
        if x not in cache:              # optimization: check for cycle length in cache
            cycle = [x]                 # if not in cache, calculate cycle
            while x > 1:
                if x % 2 == 0:          # collatz even numbers rule
                    x = x/2
                    cycle.append(x)
                else:                   # collatz odd numbers rule
                    x = (3*x) + 1
                    cycle.append(x)
            assert cycle[-1] == 1
            cache[x] = len(cycle)       # add cycle length to cache

        if cache[x] > longest:          # if cycle length is longest so far, save to longest
            longest = cache[x]

    assert longest >= 1
    return longest


# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    cache = {}                          # create cache to be used for all calculations
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j, cache)   # added cache as a parameter
        collatz_print(w, i, j, v)
